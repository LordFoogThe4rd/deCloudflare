# Kontraŭ-Tor uzantoj FQDN-listo


[//]: # (do not edit me; start)

## *2,636* FQDN

[//]: # (do not edit me; end)


- Cloudflare-domajnoj estas ekskluditaj de ĉi tiu esplorado ĉar tiuj estas sufiĉe malamikaj.
- Ne ĉiuj uzantoj de Tor estas malbonaj. Ne punu ĉiujn.
  - Kiel vi sentas, se iu blokas vin sen kialo?
  - Uzi Tor ne estas krimo.
  - [Fakuloj diras, ke gruppuno estas senutila, kontraŭproduktiva, mallaborema kaj neetika](https://web.archive.org/web/20201112000414/https://mypointexactly.wordpress.com/2009/07/21/group-punishment-ineffective-unethical/).
- Blokado de Tor ne estas solvo. Estas VPNj, retprogramoj kaj prokuroj.


-----

# Anti-Tor users FQDN list

- Cloudflare domains are excluded from this research because those are hostile enough.
- Not all Tor users are bad. Do not punish everyone.
  - How do you feel if someone block you for no reason?
  - Using Tor is not a crime.
  - Experts say that group punishment is ineffective, counterproductive, lazy and unethical.
- Blocking Tor is not a solution. There are VPNs, network program and proxies.

-----

# Using the list

  - [Karma API](../../subfiles/service/karma_api.md)
  - [API files for IsMM/IsAT Add-ons](../../tool/api_for_ismm_isat/README.md)


-----

![](../../image/anonexist.jpg)


| Date | FQDN |
| --- | --- |
| 2022-09-14 | 2,636 |
| 2022-09-13 | 2,576 |
| 2022-09-12 | 2,514 |
| 2022-09-11 | 2,425 |
| 2022-09-10 | 2,375 |
| 2022-09-09 | 2,311 |
| 2022-09-08 | 2,254 |
| 2022-09-07 | 2,188 |
| 2022-09-06 | 2,115 |
| 2022-09-05 | 2,025 |
| 2022-09-04 | 1,953 |
| 2022-09-03 | 1,905 |
| 2022-09-02 | 1,826 |
| 2022-09-01 | 1,734 |
| 2022-08-31 | 1,632 |
| 2022-08-30 | 1,474 |
| 2022-08-29 | 1,367 |
| 2022-08-28 | 1,303 |
| 2022-08-27 | 1,238 |
| 2022-08-26 | 1,142 |
| 2022-08-25 | 1,029 |
| 2022-08-24 | 907 |
| 2022-08-23 | 771 |
| 2022-08-22 | 653 |
| 2022-08-21 | 558 |
| 2022-08-20 | 477 |
| 2022-08-19 | 172 |